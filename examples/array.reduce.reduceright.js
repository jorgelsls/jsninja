var arr = [1, 3, 5, 7, 9]

var result1 = arr.reduce(function (accumulator, item, index, array) {
  return accumulator + item
}, 0)

console.log('Somatório de todos os elementos =>', result1)

var result2 = arr.reduce(function (acc, current) {
  return acc + (current % 3 ? 1 : 0)
}, 0)

console.log('Aplicando função de "normalização" =>', result2)

var name = ['J', 'o', 'r', 'g', 'e']

var result3 = name.reduceRight(function (acc, current) {
  return acc + current
}, '')

console.log('ReducRight =>', result3)
