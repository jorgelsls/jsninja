var arr = [1, 2, 3, 4, 5]

console.log('Item')

arr.forEach(function (item) {
  console.log(item)
})

console.log('Item, Índice')

arr.forEach(function (item, index) {
  console.log(item, index)
})

console.log('Item, Índice, Array')

arr.forEach(function (item, index, arr) {
  console.log(item, index, arr)
})

var sum = 0

arr.forEach(function (item) {
  sum += item;
})

console.log('Somando todos os itens no Array =>', sum)
