// Se algum dos elementos satisfizer a verificação retorna true. Caso contrário true.

var arr = [2, 3, 4, 6]

function isOdd (number) {
  return number % 2;
}

console.log('Para o Array =>', arr)

console.log('Algum elemento é impar?', arr.some(isOdd))

arr.splice(1, 1)

console.log('Para o Array =>', arr)

console.log('Algum elemento é impar?', arr.some(isOdd))
