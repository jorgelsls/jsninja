function showHelloWorldInterval() {
  console.log('Hello, world Interval')
}

function showHelloWorldTimeout() {
  console.log('Hello, world Timeout')
}

setInterval(showHelloWorldInterval, 1000)

setTimeout(showHelloWorldTimeout, 1000)
