var arr = [1, 2, 3, 4]

console.log('Array inicial =>', arr)

var arr2 = arr.concat(5)

console.log('Verificando que não houve mutação =>', arr)
console.log('Novo array criado a partir do Array inicial =>', arr2)

var arr3 = arr.concat(5, [6, 7, 8, 9])

console.log('Concatenando Array ao Array inicial =>', arr3)

var arr4 = arr.concat(1, [2, 3])

console.log('Concatenando elementos que já estavam presentes no Array Inicial =>', arr4)

var arr5 = arr.concat([6, 7, [6, 7, 8]])

console.log('Concatenando Arrays multidimensionais ao Array inicial =>', arr5)
