var obj1 = {
  'result': {
    'firstname': 'John',
    'lastname': 'Michael'
  }
}

console.log('Objeto Original =>', obj1)

var str = JSON.stringify(obj1)

console.log('String do objeto =>', str)

var obj2 = JSON.parse(str)

console.log('Novo objeto igual ao objeto original =>', obj2)

console.log('obj1 é identico a obj2?', obj1 === obj2)
