var obj = {
  x: 1,
  y: 5,
  z: 7
}

console.log('O objeto', obj)

var keys = Object.keys(obj)

console.log('Chaves do object obj =>', keys)
console.log('Quantidade de propriedades de obj =>', keys.length)

// Imprimindo todas as propriedades com for in

console.log('Imprimindo todas as propriedades com for in')

for (property in obj) {
  console.log('Valor da propriedade', property, 'é', obj[property])
}
