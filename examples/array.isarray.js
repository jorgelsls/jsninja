var arr1 = []

var arr2 = [1, 2, 3]

var something = [1, true, {}, function () { return arr1 }, 'string', NaN, null, undefined]

var isSomethingNonArrayArray = something.some(function (value) {
  return Array.isArray(value)
})

console.log(arr1)
console.log(arr2)
console.log(something)

console.log('Array vazio é Array?', Array.isArray(arr1))
console.log('Array com itens é Array?', Array.isArray(arr2))
console.log('Qualquer outra coisa é Array?', isSomethingNonArrayArray)
