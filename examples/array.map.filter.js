var arr1 = [1, 2, 3, 4, 5]

var arr2 = arr1.map(function (item, index, arr) {
  return item * 3
})

console.log('Array original =>', arr1)
console.log('Array tratado =>', arr2)

var arr3 = arr2.filter(function (item) {
  return item % 2 === 0
})

console.log('Elementos do Array tratado  que são par =>', arr3)
